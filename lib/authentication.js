const unirest = require('unirest');

function sendTokenError (res) {
  return res.status(401).send('Unable to verify token');
}

function sendLoginError (res) {
  return res.status(401).send('Invalid username or password');
}

module.exports = opts => {
  const Authentication = {};

  Authentication.register = async (req, res) => {
    const { username, password, email, firstName, lastName } = req.body;

    let createAuthRes, createUserRes;

    try {
      [createAuthRes, createUserRes] = await Promise.all([
        unirest.post(`${opts.authServiceUrl}/api/auth`).type('json').send({ username, password }),
        unirest.post(`${opts.userServiceUrl}/api/user`).type('json').send({ username, email, firstName, lastName })
      ]);
    } catch (err) {
      console.error(err);
      return res.status(500).send('Failed to register');
    }

    if (!createAuthRes || !createAuthRes.ok || !createUserRes || !createUserRes.ok || !createUserRes.body || !createUserRes.body._id) {
      // should rollback
      console.log('Failed to register user');
      return res.status(500).send('Failed to register');
    }

    const userData = createUserRes.body;

    try {
      // create jwt token from authService
      const tokenRes = await unirest
        .post(`${opts.authServiceUrl}/api/auth/signToken`)
        .type('json')
        .send({ payload: { _id: userData._id } });

      if (!tokenRes || !tokenRes.ok || !tokenRes.body || !tokenRes.body.token) {
        console.log('Failed to retrieve JWT');
        return res.status(500).send('Unable to sign token');
      }
      const { token } = tokenRes.body;

      // return token and user information to user
      res.status(200).send({ token, user: userData });
    } catch (err) {
      console.error(err);
      return res.status(500).send('Failed to retrieve JWT after registration');
    }
  };

  Authentication.login = async (req, res) => {
    const { username, password } = req.body;

    if (!username || !password) {
      console.log('login attempted, missing username or password field');
      return res.status(400).send('Missing username or password');
    }

    // call authService, send username and password, receive user access token
    try {
      const loginRes = await unirest
        .post(`${opts.authServiceUrl}/api/auth/login`)
        .type('json')
        .send({ username, password });

      if (!loginRes || !loginRes.ok || !loginRes.body) {
        console.log(`Failed on auth service login: ${username}, received ${loginRes.status}`);
        return sendLoginError(res);
      }

      const { username: authToken } = loginRes.body;

      // find user information from userService (use username for now, but a token from auth is better)
      const userRes = await unirest
        .get(`${opts.userServiceUrl}/api/user`)
        .query({ username: authToken });

      if (!userRes || !userRes.ok || !Array.isArray(userRes.body)) {
        console.log(`Unable to retrieve user information from user service for ${authToken}`);
        return sendLoginError(res);
      }

      // Using default query endpoint, receive array of users. Refactor this to a specialized function
      const userData = userRes.body[0];

      if (!userData || !userData._id) {
        console.log(`User ID not found for ${authToken}`);
        return sendLoginError(res);
      }

      // create jwt token from authService
      const tokenRes = await unirest
        .post(`${opts.authServiceUrl}/api/auth/signToken`)
        .type('json')
        .send({ payload: { _id: userData._id } });

      if (!tokenRes || !tokenRes.ok || !tokenRes.body || !tokenRes.body.token) {
        console.log('Failed to retrieve JWT');
        return res.status(500).send('Unable to sign token');
      }

      const { token } = tokenRes.body;

      // return token and user information to user
      res.status(200).send({ token, user: userData });
    } catch (err) {
      console.error(err);
      return sendLoginError(res);
    }
  };

  Authentication.logout = (req, res) => {
    // call authservice invalidate token
    res.status(200).send();
  };

  Authentication.verifyToken = async (req, res, next) => {
    const authHeader = req.get('Authorization');

    if (!authHeader) {
      return sendTokenError(res);
    }

    const splitAuth = authHeader.split(' ');
    if (Array.isArray(splitAuth) && splitAuth.length >= 2 && splitAuth[0] === 'Bearer') {
      try {
        const tokenRes = await unirest
          .post(`${opts.authServiceUrl}/api/auth/verifyToken`)
          .type('json')
          .send({ token: splitAuth[1] });
        if (tokenRes.ok && tokenRes.body && tokenRes.body.valid) {
          return next();
        }
        return sendTokenError(res);
      } catch (err) {
        return sendTokenError(res);
      }
    } else {
      return sendTokenError(res);
    }
  };

  return Authentication;
};
